import React from 'react'
import Footer from '../components/Footer'
import  Hero  from '../components/Hero'
import SelectSearch from '../components/SelectSearch'

const Home = () => {
  return (
    <>
    <Hero/>
    <SelectSearch/>
    <Footer/>

    </>
  )
}

export default Home