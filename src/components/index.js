import navBar from "./NavBar";
import bullets from "./Bullets"
import error404 from "./Error404";
import faq from "./FAQ"
import footer from "./Footer"
import hero from "./Hero"
import mediaOnRight from "./MediaOnRight"
import recentlycareers from "./RecentlyCareers"
import selectsearch from "./SelectSearch"
import stackedcards from "./StackedCards"

export{
    navBar,
    bullets,
    error404,
    faq,
    footer,
    hero,
    mediaOnRight,
    recentlycareers,
    selectsearch,
    stackedcards,
}
